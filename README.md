Delphi client for the the IMB v5 communication framework

Supported protocols:
- TCP
- TLS 1.2
- shared memory


The IMB client is released under the MIT license.